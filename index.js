const express = require('express');
const bodyParser = require('body-parser');
const connection = require('./connection.js');
const bcrypt = require('bcryptjs');

//api
const app = express();
connection.connect();
app.use(bodyParser.json());

app.post('/login', function (req, res) {
    //console.log(req.body);
    connection.query(`SELECT * FROM Users WHERE user = ?`, [req.body.user], function (error, results, fields) {
        if (error) throw error;
        if(results.length == 0){
            console.log("No existe");
            res.send('Usuario no existe');
        }else{
            //res.json(results[0]);
            let salt = bcrypt.genSaltSync(12);
            let passwordReq = bcrypt.hashSync(req.body.password, salt);
            console.log(passwordReq);
            bcrypt.compare(req.body.password,results[0].password)
            .then(doMatch=>{
                if(doMatch){
                    console.log("Soy verdadero");
                    res.json({
                        user: results[0].user,
                        password: results[0].password,
                        passwordReq: passwordReq
                    });
                }else{
                    //return res.status(422).json({error:"Password"})
                    res.send('Password incorrecto!');
                }
            }).catch(err=>{
                console.log(err);
            })
        }
    });
});
app.get('/login', function (req, res) {
  res.send('[GET]Saludos desde express');
});
app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});
